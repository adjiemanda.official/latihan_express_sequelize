const express = require('express');

const app = express();
const port = process.env.PORT || 8001;
app.use(express.json())
const { Op } = require("sequelize");

const { todo } = require('../models');
const { user } = require('../models');


  
// fungsi todos ===============================================
 

    app.get('/todos',  (req, res) => {
        todo.findAll({order: [['id', 'ASC']]})
        .then(data => { 
            res.status(200).json({
                status: true,
                message: `todos retrieved!`,
                data
            })
        })
        .catch((err) => {
            res.status(422).json({
            status: false,
            message: err.message
            })
        })
    })


    app.get('/todos/:id', (req, res) => {
        todo.findByPk(req.params.id).then(data => {
            if (data === null) {
                res.status(202).json({
                    status: false,
                    message: `todos with ID ${req.params.id} not Found!`,
                    data
                })
            } 
            else {
                res.status(200).json({
                    status: true,
                    message: `todos with ID ${req.params.id} retrieved!`,
                    data
                })
            }
        })
        .catch((err) => {
            res.status(422).json({
                status: false,
                message: err.message
            })
        }) 
    
    })
  

    app.post('/todos', (req, res) => {
        user.findOne({ where: { id: req.body.user_id } })
        .then(findIdUser => {
            if (findIdUser === null) {
                res.status(201).json({
                    status: false,
                    message: 'Can not insert todos because user id not found!',
                })
            } 
            else {
                todo.create({
                    name: req.body.name,
                    description: req.body.description,
                    due_at :req.body.due_at,
                    user_id: req.body.user_id
                })
                .then(data => {
                    res.status(201).json({
                        status: true,
                        message: 'todos created!',
                        data 
                    })
                })
                .catch((err) => {
                    res.status(422).json({
                      status: false,
                      message: err.message
                    })
                })    
            }
        })
        .catch((err) => {
            res.status(422).json({
              status: false,
              message: err.message
            })
        })
    })     


    app.delete('/todos/:id', (req, res) => {
        todo.destroy({where: {id: req.params.id}})
        .then(n => {
            if (n == 0) {
                res.status(202).json({
                    status: false,
                    message: `todos with id ${req.params.id} not found!`
                })
            }
            else {
                res.status(200).json({
                    status: true,
                    message: `${n} rows executed!! todos with id ${req.params.id} deleted!`
                })
            }
        })
        .catch((err) => {
            res.status(422).json({
                status: false,
                message: err.message
            })
        })
    })



    app.put('/todos/:id', (req, res) => {
        todo.findByPk(req.params.id)
        .then(data => {
            if (data === null) {
                res.status(202).json({
                    status: false,
                    message: `todos with ID ${req.params.id} not Found!`,
                    data
                })
            } 
            else {
                user.findOne({ where: { id: req.body.user_id } })
                .then(findIdUser => {
                    if (findIdUser === null) {
                        res.status(201).json({
                            status: false,
                            message: 'Can not update todos because user id not found!',
                        })
                    } 
                    else { 
                        todo.update({
                            name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name,
                            description: (req.body.description == null || req.body.description == "") ? data.description : req.body.description,
                            due_at: (req.body.phone == null || req.body.phone == "") ? data.due_at : req.body.due_at,
                            user_id:(req.body.user_id == null || req.body.user_id == "") ? data.user_id : req.body.user_id
                        }, {where: {id: req.params.id}}
                        )
                        .then(dataupdate => { 
                            todo.findByPk(req.params.id)
                            .then(datanew => {
                                if (dataupdate == 1 && datanew ) {
                                    res.status(200).json({
                                        status: true,
                                        message: `${dataupdate} rows executed!! todos with ID ${req.params.id} updated!`,
                                        data,
                                        datanew
                                    })
                                }
                            })
                        })
                        .catch((err) => {
                            res.status(422).json({
                                status: false,
                                message: err.message
                            })
                        }) 
                    }
                })
                .catch((err) => {
                    res.status(422).json({
                        status: false,
                        message: err.message
                    })
                })
            }
        })    
        .catch((err) => {
            res.status(422).json({
                status: false,
                message: err.message
            })
        })
    })    

// fungsi users ===============================================
  
    app.get('/users', (req, res) => {
        user.findAll({order: [['id', 'ASC']]})
        .then(data => {
            res.status(200).json({
                status: true,
                message: `users retrieved!`,
                data
            })
        })
        .catch((err) => {
            res.status(422).json({
                status: false,
                message: err.message
            })
        })
    })


    app.get('/users/:id',  (req, res) => {
        user.findByPk(req.params.id)
        .then(data => {
            if (data === null) {
                res.status(202).json({
                    status: false,
                    message: `users with ID ${req.params.id} not Found!`,
                    data
                })
            } 
            else {
                res.status(200).json({
                    status: true,
                    message: `users with ID ${req.params.id} retrieved!`,
                    data
                })
            } 
        })
        .catch((err) => {
            res.status(422).json({
                status: false,
                message: err.message
            })
        })
    })


    app.post('/users',  (req, res) => {
        user.findOrCreate({
            where: {email: req.body.email}, 
            defaults: {
                name: req.body.name,
                email: req.body.email,
                phone: req.body.phone                    
            }
        })
        .then((data) => {
            if (data[1]) {
                res.status(201).json({
                    status: true,
                    message: 'user created!',
                    data : data[0],
                  
                })
            }
            else {
                res.status(202).json({
                    status: false,
                    message: 'email already exist!',  
               })
            }
        })
        .catch((err) => {
            res.status(422).json({
                status: false,
                message: err.message
            })
        })
    })   


    app.delete('/users/:id',  (req, res) => {
        user.destroy({where: {id: req.params.id}})
        .then(n => {
            if (n == 0) {
                res.status(202).json({
                    status: false,
                    message: `user with id ${req.params.id} not found!`
                })
            }
            else {
                res.status(200).json({
                    status: true,
                    message: `${n} rows executed!! user with id ${req.params.id} deleted!`
                })
            }
        })
        .catch((err) => {
            res.status(422).json({
                status: false,
                message: err.message
            })
        })
    })
  

    app.put('/users/:id', (req, res) => {
        user.findByPk(req.params.id)
        .then(data => {
            if (data === null) {
                res.status(202).json({
                    status: false,
                    message: `users with ID ${req.params.id} not Found!`,
                    data
                })
            } 
            else {
                user.findOne({ 
                    where: { 
                      email: req.body.email, 
                      id: {
                        [Op.ne]: req.params.id
                      }
                    }
                })
                .then(findEmailUser => {
                    if (findEmailUser != null) {
                        res.status(201).json({
                            status: false,
                            message: 'Can not update users because email already exist in other user!',
                        })
                    } 
                    else {    
                        user.update({
                            name: (req.body.name == null || req.body.name == "") ? data.name : req.body.name,
                            email: (req.body.email == null || req.body.email == "") ? data.email : req.body.email,
                            phone: (req.body.phone == null || req.body.phone == "") ? data.phone : req.body.phone
                        }, {where: {id: req.params.id}}
                        )
                        .then(dataupdate => {
                            user.findByPk(req.params.id)
                            .then(datanew => {
                                if (dataupdate == 1 && datanew ) {
                                    res.status(200).json({
                                        status: true,
                                        message: `${dataupdate} rows executed!!  users with ID ${req.params.id} updated!`,
                                        data,
                                        datanew
                                    })
                                }
                            })
                            .catch((err) => {
                                res.status(422).json({
                                    status: false,
                                    message: err.message
                                })
                            })
                        })
                        .catch((err) => {
                            res.status(422).json({
                                status: false,
                                message: err.message
                            })
                        }) 
                    }
                })
                .catch((err) => {
                    res.status(422).json({
                        status: false,
                        message: err.message
                    })
                })      
            }
        })
        .catch((err) => {
            res.status(422).json({
                status: false,
                message: err.message
            })
        })
    })



app.listen(port, () => { console.log(` app listening at http://localhost:${port}`)});