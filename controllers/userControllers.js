const { user } = require('../models');
const { todo } = require('../models');
const { trainingmaster } = require('../models');
//const { historytraining } = require('../models');
const { Op } = require("sequelize");


exports.getUsers = async (req,res) => {
    try {
        const data = await user.findAll({
            attributes: ["id","username","email","phone"],
            // include: [{
            //     model: todo, 
            //     attributes: ["name","description","due_at"],
            //     as: "todos",
            // }],
            order: [['id', 'ASC']]
            });
            res.status(200).json({
                status: true,
                message: `users retrieved!`,
                data
            })
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.getUserByID = async (req, res) => {
    try {
        const data = await user.findByPk(req.params.id,{
            attributes: ["id","username","email","phone"],
            // include: [{
            //     model: todo, 
            //     attributes: ["name","description","due_at"],
            //     as: "todos",
            // }],
            order: [['id', 'ASC']]
            });
        if (data === null) {
            res.status(202).json({
                status: false,
                message: `users with ID ${req.params.id} not Found!`,
                data
            })
        } 
        else {
            res.status(200).json({
                status: true,
                message: `users with ID ${req.params.id} retrieved!`,
                data
            })
        } 
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}


exports.insertUser = async (req, res) => {
    try {
        const findEmailUser = await user.findOne({ 
            where: { 
              email: req.body.email, 
            }
        });
        if (findEmailUser != null) {
            res.status(201).json({
                status: false,
                message: 'Can not create users because email already exist in other user!',
            })
        } 
        else {
            try {
                await user.create({
                    username: req.body.username,
                    email: req.body.email,
                    password: req.body.password,
                    phone: req.body.phone
                })
                res.status(201).json({
                    status: true,
                    message: 'user created!!',
                })
            }
            catch(err) {
                res.status(422).json({
                    status: false,
                    message: err.message
                })
            }
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.updateUser = async (req, res) => {
    try {
        const data = await user.findByPk(req.params.id, {
            //attributes: ["name","email","phone","password"],
        });
        if (data === null) {
            res.status(202).json({
                status: false,
                message: `users with ID ${req.params.id} not Found!`,
                data
            })
        } 
        else {
            try {
                const findEmailUser = await user.findOne({ 
                    where: { 
                      email: req.body.email, 
                      id: {
                        [Op.ne]: req.params.id
                      }
                    }
                });
                if (findEmailUser != null) {
                    res.status(201).json({
                        status: false,
                        message: 'Can not update users because email already exist in other user!',
                    })
                } 
                else {
                    try {
                        let dataupdate = await user.update({
                            username: (req.body.username == null || req.body.username == "") ? data.username : req.body.username,
                            email: (req.body.email == null || req.body.email == "") ? data.email : req.body.email,
                            password: (req.body.password == null || req.body.password == "") ? data.password : req.body.password,
                            phone: (req.body.phone == null || req.body.phone == "") ? data.phone : req.body.phone
                        }, {where: {id: req.params.id}}
                        )
                        try {
                            const datanew = await user.findByPk(req.params.id, {
                                //attributes: ["name","email","phone","password"],
                            });
                            if (dataupdate == 1 && datanew ) {
                                res.status(200).json({
                                    status: true,
                                    message: `${dataupdate} rows executed!!  users with ID ${req.params.id} updated!`,
                                    data,
                                    datanew
                                })
                            }
                        }
                        catch(err) {
                            res.status(422).json({
                                status: false,
                                message: err.message
                            })
                        }
                    }
                    catch(err) {
                        res.status(422).json({
                            status: false,
                            message: err.message
                        })
                    }
                }
            }
            catch(err) {
                res.status(422).json({
                    status: false,
                    message: err.message
                })
            }
        } 
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.deleteUser = async (req, res) => {
    try { 
        let n = await user.destroy({where: {id: req.params.id}});
        if (n == 0) {
            res.status(202).json({
                status: false,
                message: `user with id ${req.params.id} not found!`
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: `${n} rows executed!! user with id ${req.params.id} deleted!`
            })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}


exports.getUserTodoByID = async (req,res) => {
    try {
        const data = await user.findOne({
            attributes: ["username","email","phone"],
            where: { id:req.params.id},
            include: [{model: todo, as: "todos",}]
           });
        if (data === null) {
            res.status(202).json({
                status: false,
                message: `users with ID ${req.params.id} not Found!`,
                data
            })
        } 
        else {
            let listtodos = {};
            for(index = data.todos.length - 1; index >= 0; --index) 
                {
                   listtodos[index] = data.todos[index].gettododetails
                }
            res.status(200).json({
                status: true,
                message: `users with ID ${req.params.id} retrieved!`,
                datauser : data.getuserdetails,
                listtodos,
            })
        } 
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}


exports.getUserTrainings = async (req,res) => {
    try {
        const data = await user.findAll({
            attributes: ["id","username","email","phone"],
            include: ['datadiris', {
                model: trainingmaster,
                as: 'datatrainings',
                attributes: ["id","nama_training","tanggal_training"],
                through: {
                    attributes: [],
                }
            }],
            order: [['id', 'ASC']]
            });
            res.status(200).json({
                status: true,
                message: `users retrieved!`,
                data
            })
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message,
            data
        })
    }
}

exports.getUserTrainingByID = async (req, res) => {
    try {
        const data = await user.findByPk(req.params.id,{
            attributes: ["id","username","email","phone"],
            include: ['datadiris', {
                model: trainingmaster,
                as: 'datatrainings',
                attributes: ["id","nama_training","tanggal_training"],
                through: {
                    attributes: [],
                }
            }],
            });
        if (data === null) {
            res.status(202).json({
                status: false,
                message: `users with ID ${req.params.id} not Found!`,
                data
            })
        } 
        else {
            res.status(200).json({
                status: true,
                message: `users with ID ${req.params.id} retrieved!`,
                data
            })
        } 
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}
