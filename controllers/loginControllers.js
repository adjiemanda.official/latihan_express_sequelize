const { user } = require('../models');
const JWT = require('jsonwebtoken');
const bcrypt = require('bcrypt');


exports.login = async (req, res) => {
    try {
        const findEmailUser = await user.findOne({ 
            where: { 
              email: req.body.email, 
            }
        });
        if (findEmailUser == null) {
            res.status(201).json({
                status: false,
                message: 'email not found!',
            })
        } 
        else {
            try {
                const passwordUser = findEmailUser.password;
                const passwordUserinput = req.body.password
                const comparePassword = bcrypt.compareSync(passwordUserinput, passwordUser);
                if (!comparePassword) {
                    res.status(401).json({
                      status: false,
                      message: "Login failed !!"
                    })
                  } 
                else {
                    const token = JWT.sign(
                        { id: findEmailUser.id,
                        email: findEmailUser.email },
                        process.env.SECRETKEY
                        );
                    res.status(201).json({
                        status: true,
                        message: "your email is right",
                        token
                    })
                }
            }
            catch(err) {
                res.status(422).json({
                    status: false,
                    message: "error jwt " + err.message 
                })
            }
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: req.body.email + "error find email " +  err.message
        })
    }
}