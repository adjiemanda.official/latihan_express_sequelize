const { todo } = require('../models');
const { user } = require('../models');


exports.getTodos = async (req, res) => {
    try {
        const data = await todo.findAll({
            //attributes:["activity","description","due_at"],
            order: [['id', 'ASC']]
        });
        res.status(200).json({
            status: true,
            message: `todos retrieved!`,
            data,
        })
    }
    catch(err) {
        res.status(422).json({
          status: false,
          message: err.message
        })
    }
}

exports.getTodoByID = async (req, res) => {
    try {
        const data = await todo.findByPk(req.params.id, {
            //attributes:["activity","description","due_at"],
        });
        if (data === null) {
            res.status(202).json({
                status: false,
                message: `todos with ID ${req.params.id} not Found!`,
                data
            })
        } 
        else {
            res.status(200).json({
                status: true,
                message: `todos with ID ${req.params.id} retrieved!`,
                data
            })
        } 
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.insertTodo = async (req, res) => {
    try {
        const findIdUser = await user.findOne({ where: { id: req.body.user_id } });
        if (findIdUser === null) {
            res.status(201).json({
                status: false,
                message: 'Can not insert todos because user id not found!',
            })
        } 
        else {
            try {
                const data = await todo.create({
                    activity: req.body.activity,
                    description: req.body.description,
                    due_at :req.body.due_at,
                    user_id: req.body.user_id
                })
                res.status(201).json({
                    status: true,
                    message: 'todos created!',
                    data: todo 
                })  
            }
            catch(err) {
                res.status(422).json({
                    status: false,
                    message: err.message
                })
            }
        }   
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.updateTodo = async (req, res) => {
    try { 
        const data = await todo.findByPk(req.params.id);
        if (data === null) {
            res.status(202).json({
                status: false,
                message: `todos with ID ${req.params.id} not Found!`,
                data
            })
        } 
        else {
            const findIdUser = await user.findOne({ where: { id: req.body.user_id } });
            if (findIdUser === null) {
                res.status(201).json({
                status: false,
                message: 'Can not update todos because user id not found!',
                })
            } 
            else { 
                try {
                    let dataupdate = await todo.update({
                        activity: (req.body.activity == null || req.body.activity == "") ? data.activity : req.body.activity,
                        description: (req.body.description == null || req.body.description == "") ? data.description : req.body.description,
                        due_at: (req.body.due_at == null || req.body.due_at == "") ? data.due_at : req.body.due_at,
                        user_id:(req.body.user_id == null || req.body.user_id == "") ? data.user_id : req.body.user_id
                    }, {where: {id: req.params.id}}
                    )
                    try {
                        const datanew = await todo.findByPk(req.params.id);
                        if (dataupdate == 1 && datanew ) {
                            res.status(200).json({
                                status: true,
                                message: `${dataupdate} rows executed!! todos with ID ${req.params.id} updated!`,
                                data,
                                datanew
                            })
                        }
                    }
                    catch(err) {
                        res.status(422).json({
                            status: false,
                            message: err.message
                        })
                    }    
                }
                catch(err) {
                    res.status(422).json({
                        status: false,
                        message: err.message
                    })
                }
            }
        } 
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}

exports.deleteTodo = async (req, res) => {
    try {
        let n = await todo.destroy({where: {id: req.params.id}});
        if (n == 0) {
            res.status(202).json({
                status: false,
                message: `todos with id ${req.params.id} not found!`
            })
        }
        else {
            res.status(200).json({
                status: true,
                message: `${n} rows executed!! todos with id ${req.params.id} deleted!`
            })
        }
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
  }
