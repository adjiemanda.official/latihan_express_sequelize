const { trainingmaster } = require('../models');


exports.getTrainingMasters = async (req, res) => {
    try {
        const data = await trainingmaster.findAll({
            order: [['id', 'ASC']]
        });
        res.status(200).json({
            status: true,
            message: `Trainings retrieved!`,
            data,
        })
    }
    catch(err) {
        res.status(422).json({
          status: false,
          message: err.message
        })
    }
}

exports.getTrainingMasterByID = async (req, res) => {
    try {
        const data = await trainingmaster.findByPk(req.params.id, {
        });
        if (data === null) {
            res.status(202).json({
                status: false,
                message: `Training with ID ${req.params.id} not Found!`,
                data
            })
        } 
        else {
            res.status(200).json({
                status: true,
                message: `Training with ID ${req.params.id} retrieved!`,
                data
            })
        } 
    }
    catch(err) {
        res.status(422).json({
            status: false,
            message: err.message
        })
    }
}