'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('historytrainings', [
      {
        idUser: '1',
        idTraining: '1',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        idUser: '1',
        idTraining: '2',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        idUser: '2',
        idTraining: '3',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        idUser: '2',
        idTraining: '2',
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        idUser: '3',
        idTraining: '4',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
  ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('historytrainings', null, {});
  }
};
