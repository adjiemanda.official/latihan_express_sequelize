'use strict';

module.exports = {

  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('datadiris', [
      {
      nama: 'fajri rahmanda',
      jabatan: 'Programmer',
      idUser: '1',
      createdAt: new Date(),
      updatedAt: new Date(),
      },
      {
        nama: 'Rahmanda Fajri',
        jabatan: 'Solution',
        idUser: '2',
        createdAt: new Date(),
        updatedAt: new Date(),
      }
    ]);
  
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('datadiris', null, {});
  }
};
