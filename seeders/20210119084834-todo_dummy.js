'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
   await queryInterface.bulkInsert('todos', 
    [
      {
        activity: 'belajar express',
        description: 'Mempelajari cara generate express-generator --ejs',
        due_at: '2021-02-10 13:00:00',
        idUser: '1',
        createdAt: new Date(),
        updatedAt: new Date()
    
      },
      {
        activity: 'belajar sequelize',
        description: 'Mempelajari cara generate sequelize',
        due_at: '2021-02-11 13:00:00',
        idUser: '1',
        createdAt: new Date(),
        updatedAt: new Date()
    
      },
      {
        activity: 'belajar postgressql',
        description: 'Mempelajari cara generate postgresql',
        due_at: '2021-02-13 13:00:00',
        idUser: '2',
        createdAt: new Date(),
        updatedAt: new Date()
    
      },
      {
        activity: 'belajar webtokenjson',
        description: 'Mempelajari cara generate webtokenjson',
        due_at: '2021-02-12 13:00:00',
        idUser: '3',
        createdAt: new Date(),
        updatedAt: new Date()
    
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('todos', null, {});
  }
};
