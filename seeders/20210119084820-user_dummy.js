'use strict';
const bcrypt = require('bcrypt');
const pass = 'Test1234'
const saltRounds = 10;
const passdefault = bcrypt.hashSync(pass, saltRounds);

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

  await queryInterface.bulkInsert('users', 
    [
      {
        username: 'fajri',
        email: 'fajri@test.com',
        password: passdefault,
        phone: '11-12345',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'rahmanda',
        email: 'rahmanda@test.com',
        password: passdefault,
        phone: '22-12345',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'adjie',
        email: 'adjie@test.com',
        password: passdefault,
        phone: '33-12345',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        username: 'manda',
        email: 'manda@test.com',
        password: passdefault,
        phone: '44-12345',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ]
    );
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('users', null, {});
  }
};
