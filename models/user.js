'use strict';
const {
  Model
} = require('sequelize');
const bcrypt = require('bcrypt');
const saltRounds = 10;
module.exports = (sequelize, DataTypes) => {
  class user extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here

      this.hasMany(models.todo, {
        foreignKey: "idUser", 
        as:"todos"
      })

      this.hasMany(models.datadiri, {
        foreignKey: "idUser", 
        as:"datadiris"
      })

      this.belongsToMany(models.trainingmaster, {
        through: "historytrainings",
        foreignKey: "idUser",
        as: "datatrainings"
      })
    }
  };
  user.init({
    username: {
      type: DataTypes.STRING,
      // get(username) {
      //   let rawValue = this.getDataValue(username) || "tak ada nama";
      //   let nameCapitalized = rawValue.charAt(0).toUpperCase() + rawValue.slice(1);
      //   return nameCapitalized;
      // },
      // set(username) {
      //   this.setDataValue('username',username.toLowerCase());
      // }
    },
    email: {
      type: DataTypes.STRING,
      set(email) {
        this.setDataValue('email',email.toLowerCase());
      }
    },
    password: {
      type: DataTypes.STRING,
      set(value) {
        this.setDataValue('password', bcrypt.hashSync(value, saltRounds));
      }
    },
    phone: DataTypes.STRING,
  }, {
    sequelize,
    modelName: 'user',
    // getterMethods: {
    //   getuserdetails() {
    //     return this.name + ' - ' + this.email + ' - ' + this.phone;
    //   }
    // },
  });
  return user;
};