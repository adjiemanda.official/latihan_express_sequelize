'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class historytraining extends Model {

    
    static associate(models) {
    }
    
  };
  historytraining.init({
    idUser: DataTypes.INTEGER,
    idTraining: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'historytraining',
  });
  return historytraining;
};