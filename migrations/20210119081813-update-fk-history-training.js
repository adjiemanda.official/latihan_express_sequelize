'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.changeColumn( 
     'historytrainings', 
     'idUser',
     {
      type : Sequelize.INTEGER,
      references: {
        model: 'users',
        key: 'id',
      }
     });

    await queryInterface.changeColumn( 
      'historytrainings', 
      'idTraining',
      {
       type : Sequelize.INTEGER,
       references: {
         model: 'trainingmasters',
         key: 'id',
       }
      });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.changeColumn('historytrainings','idUser',{type : Sequelize.INTEGER});
    await queryInterface.changeColumn('historytrainings','idTraining',{type : Sequelize.INTEGER});
  }
};
