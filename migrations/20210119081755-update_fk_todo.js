'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add altering commands here.
     *
     * Example:
     * await queryInterface.createTable('users', { id: Sequelize.INTEGER });
     */
    await queryInterface.changeColumn("todos", "idUser", {
      type: Sequelize.INTEGER,
      references: {
        model: "users",
        key: "id",
      },
      // onUpdate: "CASCADE",
      // onDelete: "CASCADE",
    });
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add reverting commands here.
     *
     * Example:
     * await queryInterface.dropTable('users');
     */
    await queryInterface.changeColumn("todos", "idUser", {
      type: Sequelize.INTEGER,
      // allowNull: true,
    });
  }
};
