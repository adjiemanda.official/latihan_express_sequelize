'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
     await queryInterface.addColumn(
       'datadiris', 
       'tanggal_lahir', 
       {
         type : Sequelize.DATE
       }
       );
    
  },

  down: async (queryInterface, Sequelize) => {

     await queryInterface.removeColumn('datadiris','tanggal_lahir');
     
  }
};
