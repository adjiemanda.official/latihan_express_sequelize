var express = require('express');
var router = express.Router();
const trainingControllers = require('../controllers/trainingControllers');
const passport = require('passport');


/* GET all training master list. */
router.get('/',passport.authenticate('jwt', { 'session': false }), trainingControllers.getTrainingMasters);

/* GET training master by ID */
router.get('/:id',passport.authenticate('jwt', { 'session': false }), trainingControllers.getTrainingMasterByID);


module.exports = router;
