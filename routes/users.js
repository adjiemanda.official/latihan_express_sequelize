var express = require('express');
var router = express.Router();
const userControllers = require('../controllers/userControllers');
const passport = require('passport');


/* GET all user list. */
router.get('/',passport.authenticate('jwt', { 'session': false }), userControllers.getUsers);

/* GET user list by ID */
router.get('/:id',passport.authenticate('jwt', { 'session': false }), userControllers.getUserByID);


/* POST user inserting. */
router.post('/', userControllers.insertUser);


/* PUT user updating. */
router.put('/:id',passport.authenticate('jwt', { 'session': false }), userControllers.updateUser);


/* DELETE user deleting. */
router.delete('/:id',passport.authenticate('jwt', { 'session': false }), userControllers.deleteUser);


/* GET user todos  by ID user and call getter */
router.get('/get/todos/:id',passport.authenticate('jwt', { 'session': false }), userControllers.getUserTodoByID);


/* GET all user with training. */
router.get('/get/trainings/',passport.authenticate('jwt', { 'session': false }), userControllers.getUserTrainings);


/* GET all user with training by ID user. */
router.get('/get/trainings/:id',passport.authenticate('jwt', { 'session': false }), userControllers.getUserTrainingByID);


module.exports = router;
