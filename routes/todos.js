var express = require('express');
var router = express.Router();
const todoControllers = require('../controllers/todoControllers');
const passport = require('passport');


/* GET all todo list. */
router.get('/',passport.authenticate('jwt', { 'session': false }), todoControllers.getTodos);


/* GET todo list by ID */
router.get('/:id',passport.authenticate('jwt', { 'session': false }), todoControllers.getTodoByID);


/* POST todo inserting. */
router.post('/',passport.authenticate('jwt', { 'session': false }), todoControllers.insertTodo);


/* PUT todo updating. */
router.put('/:id',passport.authenticate('jwt', { 'session': false }), todoControllers.updateTodo);


/* DELETE todo deleting. */
router.delete('/:id',passport.authenticate('jwt', { 'session': false }), todoControllers.deleteTodo);

module.exports = router;
