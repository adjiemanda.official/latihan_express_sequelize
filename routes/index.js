var express = require('express');
var router = express.Router();
const indexControllers = require('../controllers/indexControllers');
const loginControllers = require('../controllers/loginControllers');

/* GET home page. */
router.get('/', indexControllers.getLandingPage);
router.post('/api/login', loginControllers.login);

module.exports = router;
